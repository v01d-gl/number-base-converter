# Number Base Converter by v01d

*Converts string representation from one base to another*

## Installation

### From PyPI (recommended)

Via pip

```cmd
pip install --user nbc
```

### From GitLab

```cmd
git clone https://gitlab.com/v01d-gl/number-base-converter.git
cd number-base-converter
python setup.py install --user
```

## Docs

See docstring.

## Example

See doctests.
